package service;

import java.io.FileNotFoundException;

public interface TranslationInter {

    void showMenu() throws FileNotFoundException;
    void translateAzToEn();
    void translateEnToAz();
    void showWordList() throws FileNotFoundException;
    void showWordCount();
    void addNewWords();
    void exit();
}
