package service;

import java.io.*;
import java.util.Scanner;
public class TranslationImpl implements  TranslationInter{

    Scanner s=new Scanner(System.in);

    String path = "C:\\Users\\Student\\Desktop\\test.txt";
    File file=new File(path);
    Scanner scanner;

    {
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public TranslationImpl() {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


    int count=0;


    FileReader fileReader;

    {
        try {
            fileReader = new FileReader(path);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    FileWriter fileWriter;

    {
        try {
            fileWriter = new FileWriter(path,true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void showMenu() throws FileNotFoundException {
        boolean exit = false;

        while (!exit) {
            System.out.println("Сhoose one of the options:");
            System.out.println("1. Azerbaijan to English");
            System.out.println("2. English to Azerbaijan");
            System.out.println("3. Show wordlist");
            System.out.println("4. Show words count");
            System.out.println("5. Add new words");
            System.out.println("6. Exit");

            int select=s.nextInt();

            switch (select){
                case 1:
                    translateAzToEn();
                    break;
                case 2:
                    translateEnToAz();
                    break;
                case 3:
                    showWordList();
                    break;
                case 4:
                    showWordCount();
                    break;
                case 5:
                    addNewWords();

                    break;
                case 6:
                    exit = true;
                    exit();
                    break;
                default:
                    System.out.println("Error. Please try again");
            }


        }
    }


    @Override
    public void translateAzToEn() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            System.out.println("Translate word in English");
            String word = s.next();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("=>");
                String translation= parts[0].trim();
                String azWord  = parts[1].trim();

                if (azWord.equalsIgnoreCase(word)) {
                    System.out.println("In English =>  " + translation);
                    return;
                }
            }

            System.out.println("Translation word not found");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

        @Override
    public void translateEnToAz() {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                System.out.println("Translate word in Azerbaijan");
                String word = s.next();

                String line;
                while ((line = reader.readLine()) != null) {
                    String[] parts = line.split("=>");
                    String enWord = parts[0].trim();
                    String translation = parts[1].trim();

                    if (enWord.equalsIgnoreCase(word)) {
                        System.out.println("In Azerbaijan =>  " + translation);
                        return;
                    }
                }

                System.out.println("Translation word not found");

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
    }

    @Override
    public void showWordList() throws FileNotFoundException {
        Scanner ss = new Scanner(file);
        while (ss.hasNextLine()){
            System.out.println(ss.nextLine());
        }

//            BufferedReader reader = new BufferedReader(file);



//        try {
//
//            BufferedReader reader = new BufferedReader(fileReader);
//            while (reader.readLine() != null) {
//                System.out.println(reader.readLine());
//            }
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

    }

    @Override
    public void showWordCount() {
        count = 0 ;
//        while (scanner.hasNextLine()){
//            count++;
//        }
//        System.out.println("Total number of words: " + count);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));

            while (reader.readLine() != null) {
                count++;
            }

            reader.close();
            System.out.println("Total number of words: " + count);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addNewWords() {
        System.out.print("Neçə söz əlavə etmək istəyirsiniz: ");
        int countNew = s.nextInt();


            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file,true));
                for (int i = 0; i < countNew; i++) {
                    System.out.println("English word");
                    String enWord = s.next();
                    System.out.println("Azerbaijan word");
                    String azWord = s.next();
                    writer.write(enWord + " => " + azWord);
                    writer.newLine();
                }
                writer.close();
                System.out.println("New words added successfully.");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }



    @Override
    public void exit() {
        System.out.println("Log out successfully ");
    }
}
